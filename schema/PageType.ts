import { Type } from '../utils/schema';
import { UserType } from './UserType';
import { AdCampaignType } from './AdCampaignType';
import { PageCategoryType } from './PageCategoryType';

/**
 * Represents a Facebook Page.
 */
export const PageType = Type.ObjectType('Page', {
  /**
   * Page ID. No access token is required to access this field
   */
  id: {
    type: Type.String,
    core: true,
  },
  /**
   * Information about the Page
   */
  about: {
    nullable: true,
    type: Type.String,
  },
  /**
   * The Page's access token. Only returned if the User making the request has a
   * role (other than Live Contributor) on the Page. If your business requires
   * two-factor authentication, the User must also be authenticated
   */
  accessToken: {
    nullable: true,
    type: Type.String,
  },
  /**
   * The Page's currently running promotion campaign
   */
  addCampaign: {
    type: () => AdCampaignType,
  },
  /**
   * Affiliation of this person. Applicable to Pages representing people
   */
  affiliation: {
    type: Type.String,
  },
  /**
   * App ID for app-owned Pages and app Pages
   */
  appId: {
    type: Type.String,
  },
  // TODO: between fields
  /**
   * The Page's category. e.g. Product/Service, Computers/Technology
   */
  category: {
    default: true,
    type: Type.String,
  },
  /**
   * The Page's sub-categories
   */
  categoryList: {
    default: true,
    list: true,
    type: () => PageCategoryType,
  },
  name: {
    type: Type.String,
  },
});