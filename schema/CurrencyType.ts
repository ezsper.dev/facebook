import { Type } from '../utils/schema';

/**
 * A currency
 */
export const CurrencyType = Type.ObjectType('Currency', {
  /**
   * Will return a number that describes the number of additional decimal places
   * to include when displaying the person's currency. For example, the API will
   * return 100 because USD is usually displayed with two decimal places. JPY
   * does not use decimal places, so the API will return 1.
   */
  currencyOffset: {
    default: true,
    type: Type.Int,
  },
  /**
   * The exchange rate between the person's preferred currency and US Dollars
   */
  usdExchange: {
    default: true,
    type: Type.Float,
  },
  /**
   * The inverse of usdExchange
   */
  usdExchangeInverse: {
    default: true,
    type: Type.Float,
  },
  /**
   * The ISO-4217-3 code for the person's preferred currency (defaulting to USD
   * if the person hasn't set one)
   */
  userCurrency: {
    default: true,
    type: Type.String,
  },
});