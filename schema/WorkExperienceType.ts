import { Type } from '../utils/schema';
import { PageType } from './PageType';
import { UserType } from './UserType';
import { ProjectExperienceType } from './ProjectExperienceType';

/**
 * Information about a user's work
 * @deprecated This node has been deprecated as of April 4, 2018 and will
 * @deprecated return no data. Please see the changelog for more information.
 */
export const WorkExperienceType = Type.ObjectType('WorkExperience', {
  /**
   * ID
   */
  id: {
    core: true,
    type: Type.String,
  },
  /**
   * Description
   */
  description: {
    default: true,
    type: Type.String,
  },
  /**
   * Employer
   */
  employer: {
    default: true,
    type: () => PageType,
  },
  /**
   * End date
   */
  endDate: {
    default: true,
    type: Type.String,
  },
  /**
   * Tagged by
   */
  from: {
    default: true,
    type: () => UserType,
  },
  /**
   * Location
   */
  location: {
    default: true,
    type: () => PageType,
  },
  /**
   * Position
   */
  position: {
    default: true,
    type: () => PageType,
  },
  /**
   * Projects
   */
  projects: {
    default: true,
    list: true,
    type: () => ProjectExperienceType,
  },
  /**
   * Start date
   */
  startDate: {
    default: true,
    type: Type.String,
  },
  /**
   * Tagged users
   */
  with: {
    default: true,
    list: true,
    type: () => UserType,
  },
});
