import { Type } from '../utils/schema';
import { PageType } from './PageType';
import { TaskType } from './TaskType';

export const AccountType = Type.ObjectType('Account', () => [PageType], {
  tasks: {
    default: true,
    list: true,
    type: () => TaskType,
  },
});



