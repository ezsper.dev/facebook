import { Type } from '../utils/schema';
import { UserType } from './UserType';

/**
 * Information about a project experience
 */
export const ProjectExperienceType = Type.ObjectType('ProjectExperience', {
  /**
   * ID
   */
  id: {
    core: true,
    type: Type.String,
  },
  /**
   * Description
   */
  description: {
    default: true,
    type: Type.String,
  },
  /**
   * End date
   */
  endDate: {
    default: true,
    type: Type.String,
  },
  /**
   * From
   */
  from: {
    default: true,
    type: () => UserType,
  },
  /**
   * Name
   */
  name: {
    default: true,
    type: Type.String,
  },
  /**
   * Start date
   */
  startDate: {
    default: true,
    type: Type.String,
  },
  /**
   * Tagged users
   */
  with: {
    list: true,
    type: () => UserType,
  },
});



