import { Type } from '../utils/schema';

/**
 * Secure browsing settings
 */
export const SecureBrowsingType = Type.ObjectType('SecureBrowsing', {
  /**
   * Enabled
   */
  enabled: {
    default: true,
    type: Type.Boolean,
  },
});