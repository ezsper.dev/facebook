import { Type } from '../utils/schema';

/**
 * One of the following age ranges: 13–17, 18–20, or 21+
 */
export const AgeRangeType = Type.ObjectType('AgeRange', {
  /**
   * The upper bounds of the range for this person's age.
   * enum{17, 20, or empty}.
   */
  max: {
    core: true,
    nullable: true,
    type: Type.Int,
  },
  /**
   * The lower bounds of the range for this person's age.
   * enum{13, 18, 21}
   */
  min: {
    core: true,
    type: Type.Int,
  },
});