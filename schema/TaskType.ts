import { Type } from '../utils/schema';

export enum Task {
  MANAGE,
  ADVERTISE,
  ANALYZE,
  CREATE_CONTENT,
  MODERATE,
  EDIT,
}

export const TaskType = Type.EnumType('Task', Task);