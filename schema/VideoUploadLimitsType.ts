import { Type } from '../utils/schema';

/**
 * Video upload limits
 */
export const VideoUploadLimitsType = Type.ObjectType('VideoUploadLimits', {
  /**
   * Length
   */
  length: {
    default: true,
    type: Type.Int,
  },
  /**
   * Size
   */
  size: {
    default: true,
    type: Type.Int,
  },
});



