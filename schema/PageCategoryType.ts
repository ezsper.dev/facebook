import { Type } from '../utils/schema';
import { PageType } from './PageType';
import { UserType } from './UserType';
import { ProjectExperienceType } from './ProjectExperienceType';

/**
 * Page category
 */
export const PageCategoryType = Type.ObjectType('PageCategory', {
  /**
   * The id of the category.
   */
  id: {
    core: true,
    type: Type.String,
  },
  /**
   * The value to be used, in the API, for category_enum.
   */
  apiEnum: {
    default: true,
    type: Type.String,
  },
  /**
   * List of child categories.
   */
  fbPageCategories: {
    type: () => PageCategoryType,
  },
  /**
   * The name of the category.
   */
  name: {
    default: true,
    type: Type.String,
  },
});
