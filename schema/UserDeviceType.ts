import { Type } from '../utils/schema';

/**
 * Device
 */
export const UserDeviceType = Type.ObjectType('UserDevice', {
  /**
   * Hardware
   */
  hardware: {
    default: true,
    type: Type.String,
  },
  /**
   * OS
   */
  os: {
    default: true,
    type: Type.String,
  },
});