import { Type } from '../utils/schema';
import { SecureBrowsingType } from './SecureBrowsingType';

/**
 * Security settings
 */
export const SecuritySettingsType = Type.ObjectType('SecuritySettings', {
  /**
   * Secure browsing settings
   */
  secureBrowsing: {
    default: true,
    type: () => SecureBrowsingType,
  },
});