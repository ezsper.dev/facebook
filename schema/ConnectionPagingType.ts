import { Type } from '../utils/schema';
import { ConnectionPagingCursorType } from './ConnectionPagingCursorType';

export const ConnectionPagingType = Type.ObjectType('ConnectionPagingType', {
  cursors: {
    core: true,
    type: ConnectionPagingCursorType,
  },
  previous: {
    core: true,
    nullable: true,
    type: Type.String,
  },
  after: {
    core: true,
    nullable: true,
    type: Type.String,
  },
});