import { Type } from '../utils/schema';
import { UserType } from './UserType';
import { PageType } from './PageType';

/**
 * An experience
 * TODO: the other types
 */
export const ProfileType = Type.ObjectType('Profile', () => {
  return [
    PageType,
    UserType,
  ];
});