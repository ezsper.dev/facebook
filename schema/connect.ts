import {
  Type,
  SchemaObjectOutputType,
  SchemaOutputTypeFieldDefinition, Omit
} from '../utils/schema';
import { SummaryType } from './SummaryType';
import { ConnectionPagingType } from './ConnectionPagingType';
import { ConnectionSummaryType } from './ConnectionSummaryType';

export function injectModifiers<
  F extends SchemaOutputTypeFieldDefinition,
  M extends SchemaOutputTypeFieldDefinition['modifiers']
>(field: F, modifiers: M): Omit<F, 'modifiers'> & { modifiers: F extends { modifiers: any } ? F['modifiers'] & M : M } {
  const newField = Object.assign({}, field);
  newField.modifiers = Object.assign({}, newField.modifiers, modifiers);
  return <any>newField;
}

/**
 * Connects a type
 * @param type
 */
export const connect = <T extends SchemaObjectOutputType>(type: T) => {
  return {
    modifiers: Type.modifiers({
      limit: {
        nullable: true,
        type: Type.Int,
      },
      summary: {
        nullable: true,
        list: true,
        type: SummaryType,
      },
    }),
    type: () => (
      Type.ConnectionObjectType(`Connection${type.name}`, {
        data: {
          type: () => type,
          core: true,
          list: true,
        },
        paging: {
          core: true,
          type: ConnectionPagingType,
        },
        summary: {
          type: ConnectionSummaryType,
        },
      })
    ),
  };
};