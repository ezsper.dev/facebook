import { Type } from '../utils/schema';

export const ConnectionSummaryType = Type.ObjectType('ConnectionSummary', {
  totalCount: {
    nullable: true,
    type: Type.Int,
  },
});