import { Type } from '../utils/schema';
import { UserType } from './UserType';
import { PageType } from './PageType';

export const PageAdminNoteType = Type.ObjectType('PageAdminNote', {
  body: {
    type: Type.String,
  },
  from: {
    core: true,
    type: () => PageType,
  },
  id: {
    default: true,
    type: Type.String,
  },
  address: {
    nullable: true,
    type: Type.String,
  },
  user: {
    type: () => UserType,
  },
});