import { Type } from '../utils/schema';
import { PageType } from './PageType';
import { ProfileType } from './ProfileType';
import { connect } from './connect';
import { UserType } from './UserType';

/**
 * Page's label
 */
export const PageLabelType = Type.ObjectType('PageLabel', {
  /**
   * Time when the label was created
   */
  createtionTime: {
    type: Type.DateTime,
  },
  /**
   * Admin who created the label
   */
  creatorId: {
    type: () => ProfileType,
  },
  /**
   * Page that owns the label
   */
  from: {
    type: () => PageType,
  },
  /**
   * ID of the label
   */
  id: {
    core: true,
    type: Type.String,
  },
  /**
   * Name of the label
   */
  name: {
    default: true,
    type: Type.String,
  },
  /**
   * Users that are associated with this label
   */
  users: connect(UserType),
});