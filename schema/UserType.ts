import { Type } from '../utils/schema';
import { connect, injectModifiers } from './connect';
import { PageAdminNoteType } from './PageAdminNoteType';
import { AccountType } from './AccountType';
import { AgeRangeType } from './AgeRangeType';
import { UserContextType } from './UserContextType';
import { UserCoverPhotoType } from './UserCoverPhotoType';
import { CurrencyType } from './CurrencyType';
import { UserDeviceType } from './UserDeviceType';
import { ExperienceType } from './ExperienceType';
import { PageType } from './PageType';
import { PageLabelType } from './PageLabelType';
import { PaymentPricepointsType } from './PaymentPricepointsType';
import { SecuritySettingsType } from './SecuritySettingsType';
import { VideoUploadLimitsType } from './VideoUploadLimitsType';
import { WorkExperienceType } from './WorkExperienceType';

/**
 * A user represents a person on Facebook.
 * [1] relative to application
 */
export const UserType = Type.ObjectType('User', {
  /**
   * The user about
   * @deprecated Returns no data as of April 4, 2018.
   */
  about: {
    nullable: true,
    type: Type.String,
  },
  /**
   * The id of this person's user account. This ID is unique to each app and
   * cannot be used across different apps.
   */
  id: {
    core: true,
    type: Type.String,
  },
  /**
   * The User's address.
   */
  address: {
    nullable: true,
    type: Type.String,
  },
  /**
   * Notes added by viewing page on this User.
   */
  adminNotes: {
    list: true,
    type: () => PageAdminNoteType,
  },
  /**
   * The age segment for this person expressed as a minimum and maximum age.
   * For example, more than 18, less than 21.
   */
  ageRange: {
    nullable: true,
    type: () => AgeRangeType,
  },
  /**
   * The person's birthday. This is a fixed format string, like MM/DD/YYYY.
   * However, people can control who can see the year they were born separately
   * from the month and day so this string can be only the year (YYYY) or the
   * month + day (MM/DD)
   */
  birthday: {
    nullable: true,
    type: Type.String,
  },
  /**
   * Can the person review brand polls [1]
   */
  canReviewMeasurementRequest: {
    type: Type.Boolean,
  },
  /**
   * Social context for this person
   */
  context: {
    type: () => UserContextType,
  },
  /**
   * The person's cover photo
   * @deprecated Obsolete
   */
  cover: {
    nullable: true,
    type: () => UserCoverPhotoType,
  },
  /**
   * The person's local currency information
   * @deprecated Obsolete
   */
  currency: {
    nullable: true,
    type: () => CurrencyType,
  },
  /**
   * The list of devices the person is using. This will return only iOS and
   * Android devices
   * @deprecated Obsolete
   */
  devices: {
    nullable: true,
    type: () => UserDeviceType,
  },
  /**
   * Users education
   * @deprecated Returns no data as of April 4, 2018.
   */
  education: {
    nullable: true,
    type: () => ExperienceType,
  },
  /**
   * The User's primary email address listed on their profile. This field will
   * not be returned if no valid email address is available.
   */
  email: {
    nullable: true,
    type: Type.String,
  },
  /**
   * The User's employee number, as set by the company via SCIM API.
   */
  employeeNumber: {
    nullable: true,
    type: Type.String,
  },
  /**
   * Athletes the User likes.
   */
  favoriteAthletes: {
    list: true,
    type: () => ExperienceType,
  },
  /**
   * Athletes the User likes.
   */
  favoriteTeams: {
    list: true,
    type: () => ExperienceType,
  },
  /**
   * The person's first name
   */
  firstName: {
    type: Type.String,
  },
  /**
   * The gender selected by this person, male or female. If the gender is set to
   * a custom value, this value will be based off of the preferred pronoun; it
   * will be omitted if the preferred preferred pronoun is neutral
   */
  gender: {
    nullable: true,
    type: Type.String,
  },
  /**
   * The person's hometown
   */
  hometown: {
    nullable: true,
    type: () => PageType,
  },
  /**
   * The person's inspirational people
   */
  inspirationalPeople: {
    list: true,
    type: () => ExperienceType,
  },
  /**
   * Install type [1]
   */
  installType: {
    // TODO: Create proper enum
    type: Type.String,
  },
  /**
   * Is the app making the request installed
   */
  installed: {
    type: Type.Boolean,
  },
  /**
   * @deprecated Returns no data as of April 4, 2018.
   */
  interestedIn: {
    list: true,
    type: Type.String,
  },
  /**
   * If the user has used FAME deeplinking
   */
  isFamedeeplinkinguser: {
    type: Type.Boolean,
  },
  /**
   * Is this a shared login (e.g. a gray user)
   */
  isSharedLogin: {
    type: Type.Boolean,
  },
  /**
   * People with large numbers of followers can have the authenticity of their
   * identity manually verified by Facebook. This field indicates whether the
   * person's profile is verified in this way. This is distinct from the
   * verified field
   * @deprecated Obsolete
   */
  isVerified: {
    type: Type.Boolean,
  },
  /**
   * Labels applied by viewing page on this person
   */
  labels: {
    list: true,
    type: () => PageLabelType,
  },
  /**
   * Facebook Pages representing the languages this person knows
   */
  languages: {
    list: true,
    type: () => ExperienceType,
  },
  /**
   * The person's last name
   */
  lastName: {
    type: Type.String,
  },
  /**
   * A link to the person's Timeline. The link will only resolve if the person
   * clicking the link is logged into Facebook and is a friend of the person
   * whose profile is being viewed.
   */
  link: {
    type: Type.String,
  },
  /**
   * Display megaphone for local news bookmark
   * @deprecated Obsoleto
   */
  localNewsMegaphoneDismissStatus: {
    type: Type.Boolean,
  },
  /**
   * Daily local news notification
   * @deprecated Obsoleto
   */
  localNewsSubscriptionStatus: {
    type: Type.Boolean,
  },
  /**
   * The person's locale
   * @deprecated Obsoleto
   */
  locale: {
    type: Type.String,
  },
  /**
   * The person's current location as entered by them on their profile. This
   * field is not related to check-ins
   */
  location: {
    type: () => PageType,
  },
  /**
   * What the person is interested in meeting for
   */
  meetingFor: {
    list: true,
    type: Type.String,
  },
  /**
   * The person's middle name
   */
  middleName: {
    type: Type.String,
  },
  /**
   * The person's full name
   */
  name: {
    default: true,
    type: Type.String,
  },
  /**
   * The person's name formatted to correctly handle Chinese, Japanese, or
   * Korean ordering
   */
  nameFormat: {
    type: Type.String,
  },
  /**
   * The person's payment pricepoints [1]
   */
  paymentPricepoints: {
    type: () => PaymentPricepointsType,
  },
  /**
   * @deprecated Returns no data as of April 4, 2018.
   */
  political: {
    type: Type.String,
  },
  /**
   * The profile picture URL of the Messenger user. The URL will expire.
   */
  profilePic: {
    type: Type.String,
  },
  /**
   * The person's PGP public key
   */
  publicKey: {
    type: Type.String,
  },
  /**
   * The person's favorite quotes
   */
  quotes: {
    type: Type.String,
  },
  /**
   * @deprecated Returns no data as of April 4, 2018.
   */
  relationshipStatus: {
    type: Type.String,
  },
  /**
   * @deprecated Returns no data as of April 4, 2018.
   */
  religion: {
    type: Type.String,
  },
  /**
   * Security settings
   */
  securitySettings: {
    type: SecuritySettingsType,
  },
  /**
   * The time that the shared loginneeds to be upgraded to Business Manager by
   */
  sharedLoginUpgradeRequiredBy: {
    type: Type.DateTime,
  },
  /**
   * Shortened, locale-aware name for the person [1]
   */
  shortName: {
    type: Type.String,
  },
  /**
   * The person's significant other
   */
  significantOther: {
    type: () => UserType,
  },
  /**
   * Sports played by the person
   */
  sports: {
    list: true,
    type: () => ExperienceType,
  },
  /**
   * Platform test group [1]
   */
  testGroup: {
    type: Type.Int,
  },
  /**
   * A string containing an anonymous, unique identifier for the User, for use
   * with third-parties. Deprecated for versions 3.0+. Apps using older versions
   * of the API can get this field until January 8, 2019. Apps installed by the
   * User on or after May 1st, 2018, cannot get this field.
   * @deprecated Obsolete
   */
  thirdPartyId: {
    nullable: true,
    type: Type.String,
  },
  /**
   * The person's current timezone offset from UTC
   * @deprecated Obsolete
   */
  timezone: {
    nullable: true,
    type: Type.Float,
  },
  /**
   * A token that is the same across a business's apps. Access to this token
   * requires that the person be logged into your app or have a role on your
   * app. This token will change if the business owning the app changes
   */
  tokenForBusiness: {
    type: Type.String,
  },
  /**
   * Updated time
   * @deprecated Obsolete
   */
  updatedTime: {
    nullable: true,
    type: Type.DateTime,
  },
  /**
   * ndicates whether the account has been verified. This is distinct from the
   * isVerified field. Someone is considered verified if they take any of the
   * following actions:
   * ---- removed ----
   * @deprecated Obsolete
   */
  verified: {
    nullable: true,
    type: Type.Boolean,
  },
  /**
   * Video upload limits
   */
  videoUploadLimits: {
    type: () => VideoUploadLimitsType,
  },
  /**
   * Can the viewer send a gift to this person? [1]
   */
  viewerCanSendGift: {
    type: Type.Boolean,
  },
  /**
   * @deprecated Returns no data as of April 4, 2018.
   */
  website: {
    type: Type.String,
  },
  /**
   * @deprecated Returns no data as of April 4, 2018.
   */
  work: {
    nullable: true,
    list: true,
    type: () => WorkExperienceType,
  },
  accounts: injectModifiers(connect(AccountType), {
    /**
     * Filter pages by a specific business id
     */
    businessId: {
      nullable: true,
      type: Type.String,
    },
    /**
     * If specified, filter pages based on whetherthey are associated with a
     * Business manager or not
     */
    isBusiness: {
      nullable: true,
      type: Type.Boolean,
    },
    /**
     * If specified,filter pages based on whetherthey are places or not
     */
    IsPlace: {
      nullable: true,
      type: Type.Boolean,
    },
    /**
     * If specified, filter pages based on whether they can be promoted or not
     */
    isPromotable: {
      nullable: true,
      type: Type.Boolean,
    },
  }),
});
