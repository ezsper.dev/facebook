import { Type } from '../utils/schema';
import { ExperienceType } from './ExperienceType';
import { PageType } from './PageType';
import { UserType } from './UserType';

/**
 * The person's education history
 */
export const EducationExperienceType = Type.ObjectType('EducationExperience', {
  /**
   * ID
   */
  id: {
    type: Type.String,
  },
  /**
   * Classes taken
   */
  classes: {
    default: true,
    list: true,
    type: () => ExperienceType,
  },
  /**
   * Facebook Pages representing subjects studied
   */
  concentration: {
    default: true,
    list: true,
    type: () => PageType,
  },
  /**
   * The Facebook Page for the degree obtained
   */
  degree: {
    default: true,
    type: () => PageType,
  },
  /**
   * The Facebook Page for this school
   */
  school: {
    default: true,
    type: () => PageType,
  },
  /**
   * The type of educational institution
   */
  type: {
    default: true,
    type: Type.String,
  },
  /**
   * People tagged who went to school with this person
   */
  with: {
    default: true,
    list: true,
    type: () => UserType,
  },
  /**
   * Facebook Page for the year this person graduated
   */
  year: {
    default: true,
    list: true,
    type: () => PageType,
  },
});