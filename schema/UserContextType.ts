import { Type } from '../utils/schema';
import { PageType } from './PageType';
import { connect } from './connect';

/**
 * Social context for a person
 */
export const UserContextType = Type.ObjectType('UserContext', {
  /**
   * The token representing the social context
   */
  id: {
    default: true,
    type: Type.String,
  },
  /**
   * Social context edge providing a list of the liked Pages that the calling
   * person and the target person have in common
   */
  mutualLikes: {
    default: true,
    ...connect(PageType),
  },
});
