import { Type } from '../utils/schema';
import { UserType } from './UserType';

/**
 * An experience
 */
export const ExperienceType = Type.ObjectType('Experience', {
  /**
   * ID
   */
  id: {
    default: true,
    type: Type.String,
  },
  /**
   * Description
   */
  description: {
    default: true,
    type: Type.String,
  },
  /**
   * From
   */
  from: {
    default: true,
    type: () => UserType,
  },
  /**
   * Name
   */
  name: {
    default: true,
    type: Type.String,
  },
  /**
   * Tagged users
   */
  with: {
    default: true,
    list: true,
    type: () => UserType,
  },
});