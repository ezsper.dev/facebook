import { Type } from '../utils/schema';

/**
 * An ad set is a group of ads that share the same daily or lifetime budget, schedule, bid type, bid info, and targeting
 * data. Ad sets enable you to group ads according to your criteria, and you can retrieve the ad-related statistics that
 * apply to a set.
 */
export const AdCampaignType = Type.ObjectType('AdCapaign', {
  /**
   * Ad set ID
   */
  id: {
    type: Type.String,
    core: true,
  },
  accountId: {
    type: Type.String,
  },
  // TODO: The rest of the fields
});