import { Type } from '../utils/schema';

export enum Summary {
  totalCount,
}

export const SummaryType = Type.EnumType('Summary', Summary);