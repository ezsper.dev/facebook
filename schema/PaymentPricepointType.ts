import { Type } from '../utils/schema';

/**
 * A payment pricepoint
 */
export const PaymentPricepointType = Type.ObjectType('PaymentPricepoint', {
  /**
   * Credits
   */
  credits: {
    default: true,
    type: Type.Float,
  },
  /**
   * Local currency
   */
  localCurrency: {
    default: true,
    type: Type.String,
  },
  /**
   * User price
   */
  userPrice: {
    default: true,
    type: Type.String,
  },
});