import { Type } from '../utils/schema';

/**
 * Cover photo
 */
export const UserCoverPhotoType = Type.ObjectType('UserCoverPhoto', {
  /**
   * When greater than 0% but less than 100%, the cover photo overflows
   * horizontally. The value represents the horizontal manual offset
   * (the amount the user dragged the photo horizontally to show the part of
   * interest) as a percentage of the offset necessary to make the photo fit the
   * space.
   */
  offsetX: {
    default: true,
    type: Type.Float,
  },
  /**
   * When greater than 0% but less than 100%, the cover photo overflows
   * vertically. The value represents the vertical manual offset (the amount the
   * user dragged the photo vertically to show the part of interest) as a
   * percentage of the offset necessary to make the photo fit the space.
   */
  offsetY: {
    default: true,
    type: Type.Float,
  },
  /**
   * Direct URL for the person's cover photo image
   */
  source: {
    default: true,
    type: Type.String,
  },
});