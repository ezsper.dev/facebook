import { Type } from '../utils/schema';

export const ConnectionPagingCursorType = Type.ObjectType('ConnectionPagingCursor2', {
  before: {
    core: true,
    nullable: true,
    type: Type.String,
  },
  after: {
    core: true,
    nullable: true,
    type: Type.String,
  },
});