import { Type } from '../utils/schema';
import { PaymentPricepointType } from './PaymentPricepointType';

/**
 * Payment pricepoints
 */
export const PaymentPricepointsType = Type.ObjectType('PaymentPricepoints', {
  /**
   * Mobile payment pricepoints
   */
  mobile: {
    default: true,
    type: () => PaymentPricepointType,
  },
});