import { Response } from 'superagent';

export class GraphError extends Error {

  constructor(
    message: string,
    readonly type: string,
    readonly fbCode: number,
    readonly fbTraceId: string,
    readonly statusCode: number,
    readonly data: any,
    readonly response: Response,
    readonly originalError?: Error,
  ) {
    super();
    this.name = 'FacebookGraphError';
    this.message = message;
  }

}