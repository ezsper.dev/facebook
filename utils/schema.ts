import { $enum } from 'ts-enum-util';
/**
 * Used internally to verify that some type is enum-like.
 * A type is enum-like if all its properties are of type number or string.
 * @template V - Type of the enum value.
 * @template K - String literal union of all keys of the enum-like type.
 */

export declare type EnumLike<V extends number | string, K extends string> = {
  [P in K]: V;
};

/**
 * Extracts only keys of type T that are assignable to type `string`.
 * This is necessary starting with TypeScript 2.9 because keyof T can now
 * include `number` and `symbol` types.
 */
export type StringKeyOf<T> = Extract<keyof T, string>;

/**
 * Utility type that matches values of an object and return the matching keys
 */
export type MatchValues<T, V> = Extract<{ [K in keyof T]: { key: K, type: T[K] } }[keyof T], { key: string, type: V }> extends { key: infer K }
  ? (K extends keyof T ? K : never)
  : never;

/**
 * Utility type that omit keys of a type
 */
export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

export type IsNever<T> = T extends never ? true : false;

/**
 * Utility type that make specified fields required and the rest optional
 */
export type RequiredKeys<T, R extends keyof T> = (
  { map: { [K in R]-?: T[K] } & { [K in Exclude<keyof T, R>]?: T[K] } }
) extends { map: infer U }
  ? { [K in keyof U]: U[K] }
  : never;

/**
 * Utility type that optionalize object keys that have optional values
 */
export type Optionalize<T> = RequiredKeys<T, MatchValues<{
  [K in keyof T]: T[K] extends never
    ? false
    : (
      Extract<T[K], undefined> extends never
        ? (
          T[K] extends {}
            ? (
              keyof T[K] extends never
                ? false
                : (
                  Partial<T[K]> extends T[K]
                    ? false
                    : true
                )
            )
            : true
        )
        : false
    )
}, true>>;

/**
 * Utility type that make specified fields optional and the rest required
 */
export type PartialKeys<T, R extends keyof T> = RequiredKeys<T, Exclude<keyof T, R>>;

/**
 * Wrap a value inside a function
 */
export type Thunk<T> = (() => T) | T;

/**
 * Schema scalar type parser map for encode and decode
 */
export type SchemaScalarTypeConfig<T = any, O = any> = {
  /**
   * Decode from Facebook Graph JSON type to JavaScript type
   * @param value
   */
  decode: (value: O) => T;
  /**
   * Encode from Javascript type to Facebook Graph JSON type
   * @param value
   */
  encode: (value: T) => O;
};

/**
 * All scalar types mapped to their parsers
 */
export type SchemaScalarTypeMap = {
  [K in keyof typeof schemaScalarTypes]: SchemaScalarTypeConfig;
}

/**
 * All singular scalar types accepted on schema
 */
export type SchemaScalarType = keyof SchemaScalarTypeMap;

export type SchemaObjectTypeFieldValue<T extends SchemaFieldDefinition<any> = SchemaFieldDefinition<any>> =
  T extends SchemaFieldDefinition<infer V>
    ? Thunk<Extract<V, string>> | T
    : never;

/**
 * Inferable schema field map
 */
export type SchemaObjectTypeFieldMap<T extends SchemaObjectTypeFieldValue<any> = SchemaObjectTypeFieldValue<any>> = Record<string, T>;

/**
 * Schema enum type
 */
export interface SchemaEnumType<T extends EnumLike<number | string, StringKeyOf<T>> = any> {
  name: string;
  enum: T;
}

/**
 * Schema natural object type
 */
export interface SchemaObjectType<F extends SchemaObjectTypeFieldMap<any> = SchemaObjectTypeFieldMap<any>> {
  name: string;
  fields: F;
}

/**
 * Schema object output type field map
 */
export type SchemaObjectOutputTypeFieldMap<T = any> =
  SchemaObjectTypeFieldMap<SchemaOutputTypeFieldDefinition<T>>;

/**
 * Schema object output type field map for connection objects
 */
export type SchemaObjectConnectionOutputTypeFieldMap<T = any> =
  SchemaObjectOutputTypeFieldMap<T> & {
    data: SchemaOutputTypeFieldDefinition<T> & { list: true }
  };

export type SchemaObjectInputTypeFieldMap<T = any> =
  SchemaObjectTypeFieldMap<SchemaInputTypeFieldDefinition<T>>;

/**
 * Schema object type
 */
export type SchemaObjectOutputType<T = any> = SchemaCommonObjectOutputType<T> | SchemaObjectConnectionOutputType<T>;

/**
 * Schema normal object output type (not an edge)
 */
export interface SchemaCommonObjectOutputType<T = any> extends SchemaObjectType<SchemaObjectOutputTypeFieldMap<T>> {
  connection?: false;
}

/**
 * Schema connection object output type
 */
export interface SchemaObjectConnectionOutputType<T = any> extends SchemaObjectType<SchemaObjectOutputTypeFieldMap<T>> {
  connection: true;
  fields: SchemaObjectConnectionOutputTypeFieldMap<T>;
}

/**
 * Schema output type
 */
export type SchemaOutputType = SchemaScalarType | SchemaEnumType | SchemaObjectOutputType;

/**
 * Schema object input type
 */
export interface SchemaObjectInputType<T = any> extends SchemaObjectType<SchemaObjectInputTypeFieldMap<T>> {}

/**
 * Schema input type
 */
export type SchemaInputType = SchemaScalarType | SchemaEnumType | SchemaObjectInputType;

/**
 * Schema field type definition
 */
export interface SchemaFieldDefinition<T = any> {
  /**
   * Rather the field is nullable
   */
  nullable?: true | false;
  /**
   * Rather the field is a list
   */
  list?: true | false;
  /**
   * Rather the field item is nullable
   */
  nullableItem?: this extends { list: true }
    ? true | false
    : false;
  /**
   * The field type
   */
  type: Thunk<T>;
}

/**
 * Schema input type field definition
 */
export interface SchemaInputTypeFieldDefinition<T = any> extends SchemaFieldDefinition<T> {}

/**
 * Schema field definition
 */
export interface SchemaOutputTypeFieldDefinition<T = any> extends SchemaFieldDefinition<T> {
  /**
   * If fields is present even if not selected
   */
  core?: true | false;
  /**
   * If field is present when no field is selected
   */
  default?: true | false;
  /**
   * The field modifiers
   */
  modifiers?: SchemaObjectInputTypeFieldMap<T>;
}

/**
 * Transforms schema scalar type to it's JavaScript type
 */
export type TransformSchemaScalarType<STM extends SchemaScalarTypeMap, K extends SchemaScalarType, DECODE extends true | false = true> =
  STM[K] extends SchemaScalarTypeConfig
    ? ReturnType<(DECODE extends true ? STM[K]['decode'] : STM[K]['encode'])>
    : never;

/**
 * Transforms schema nullable item to JavaScript type
 */
export type TransformSchemaListItemType<T, S> = S extends { list: true, nullableItem: true }
  ? T | undefined
  : T;

/**
 * Transforms schema list to javascript type
 */
export type TransformSchemaListType<T, S> = S extends { list: true }
  ? TransformSchemaListItemType<T, S>[]
  : T;

/**
 * Transforms schema nullable to javascript type
 */
export type TransformSchemaNullableType<T, S> = S extends { nullable: true }
  ? TransformSchemaListType<T, S> | undefined
  : TransformSchemaListType<T, S>;

/**
 * Transforms schema field
 */
export type TransformSchemaFieldType<STM extends SchemaScalarTypeMap, S, DECODE extends true | false = true> =
  S extends Thunk<SchemaScalarType>
    ? TransformSchemaType<STM, S, DECODE>
    : (
      S extends SchemaFieldDefinition<infer T>
        ? TransformSchemaNullableType<TransformSchemaType<STM, T, DECODE>, S>
        : never
      );

export type TransformSchemaFieldMapType<STM extends SchemaScalarTypeMap, F, DECODE extends true | false = true> =
  F extends SchemaObjectTypeFieldMap
    ? PartialKeys<{ [K in keyof F]: TransformSchemaFieldType<STM, F[K], DECODE>; }, MatchValues<F, { nullable: true }>>
    : never;

/**
 * Transforms any schema type to it's javascript single type
 */
export type TransformSchemaType<STM extends SchemaScalarTypeMap, S, DECODE extends true | false = true> =
  S extends Thunk<infer S>
    ? (
      S extends SchemaScalarType
        ? TransformSchemaScalarType<STM, S, DECODE>
        : (
          S extends SchemaEnumType
            ? keyof S['enum']
            : (
              S extends SchemaObjectType<infer F>
                // TODO: Partial keys is the problem
                ? TransformSchemaFieldMapType<STM, F, DECODE>
                : never
            )
        )
    )
    : never;

/**
 * Format schema selection optional
 */
/*
export type FormatSchemaExecutionOptional<M, F> =
  (null | Partial<FormatSchemaExecutionRequired<M, F>>);
*/
/**
 * Format schema selection required
 */

const pip: { a: 1 } extends { a?: 1 } ? 1 : 2 = 1;

const a: FormatSchemaExecution<{
  modifiers: {
    limit?: number;
  },
  fields: {
    a?: FormatSchemaExecution<{ modifiers: never, fields: never }, true>;
  },
}, false> = <any>{};

export type FormatSchemaExecution<T extends { modifiers: any, fields: any }, SUB extends true | false = false> =
  ({ format: Optionalize<T> }) extends { format: infer U }
    ? (
      SUB extends true
        ? (
          Partial<U> extends U
            ? (SUB extends true ? (null | U) : U)
            : U
        )
        : U
    )
    : never;
/*
  T extends { modifiers: infer M, fields: infer F }
    ? (
      ({
        format: (
          (
            Partial<M> extends M
              ? { modifiers?: M | null }
              : { modifiers: M }
            )
          & (
          Partial<F> extends F
            ? { fields?: F | null }
            : { fields: F }
          )
          )
      }) extends { format: infer U }
        ? (
          SUB extends true
            ? (
              Partial<U> extends U
                ? (null | { [K in keyof U]: U[K] })
                : { [K in keyof U]: U[K] }
              )
            : { [K in keyof U]: U[K] }
          )
        : never
    )
    : never
    */;

/**
 * Format schema selection
 */
/*
export type FormatSchemaExecution<M, F> =
  keyof M extends never
    ? (
      keyof F extends never
        ? FormatSchemaExecutionOptional<never, never>
        : (
          Partial<F> extends F
            ? FormatSchemaExecutionOptional<never, F>
            : RequiredKeys<FormatSchemaExecutionRequired<never, F>, 'fields'>
        )
      )
    : (
      Partial<M> extends M
        ? (
          keyof F extends never
            ? FormatSchemaExecutionOptional<M, never>
            : (
              Partial<F> extends F
                ? FormatSchemaExecutionOptional<never, F>
                : RequiredKeys<FormatSchemaExecutionRequired<never, F>, 'fields'>
              )
          )
        : (
          keyof F extends never
            ? RequiredKeys<FormatSchemaExecutionRequired<M, never>, 'modifiers'>
            : (
              Partial<F> extends F
                ? RequiredKeys<FormatSchemaExecutionRequired<M, F>, 'modifiers'>
                : FormatSchemaExecutionRequired<M, F>
              )
          )
    );
*/

/**
 * Schema modifiers selection
 */
export type SchemaModifierExecution<STM extends SchemaScalarTypeMap, M> =
  keyof M extends never
    ? never
    : TransformSchemaFieldMapType<STM, M>;

/**
 * Schema field selection
 */
export type SchemaFieldExecution<STM extends SchemaScalarTypeMap, S, SUB extends true | false = false> =
  S extends Thunk<SchemaScalarType>
    ? SchemaExecution<STM, S, never, SUB>
    : (
      S extends SchemaFieldDefinition<infer T>
        ? (
          S extends { modifiers: infer M }
            ? SchemaExecution<STM, T, M, SUB>
            : SchemaExecution<STM, T, never, SUB>
          )
        : never
      );

/**
 * Schema selection
 */
export type SchemaExecution<STM extends SchemaScalarTypeMap, S, M = never, SUB extends true | false = false> =
  S extends Thunk<infer S>
    ? (
      S extends SchemaScalarType | SchemaEnumType
        ? FormatSchemaExecution<{ modifiers: SchemaModifierExecution<STM, M>, fields: never }, SUB>
        : (
          S extends { connection: true, fields: { data: infer D } }
            ? (
              D extends { type: Thunk<SchemaObjectType<infer F>> }
                ? FormatSchemaExecution<{ modifiers: SchemaModifierExecution<STM, M>, fields: RequiredKeys<{ [K in keyof F]: SchemaFieldExecution<STM, F[K], true> }, MatchValues<F, { required: true }>> }, SUB>
                : FormatSchemaExecution<{ modifiers: SchemaModifierExecution<STM, M>, fields: never }, SUB>
            )
            : (
              S extends SchemaObjectType<infer F>
                ? FormatSchemaExecution<{ modifiers: SchemaModifierExecution<STM, M>, fields: RequiredKeys<{ [K in keyof F]: SchemaFieldExecution<STM, F[K], true> }, MatchValues<F, { required: true }>> }, SUB>
                : never
              )
          )
    )
    : never;


export type SchemaObjectTypeFieldMapFilterRecursion<STM extends SchemaScalarTypeMap, F, M, MM> =
  F extends SchemaObjectTypeFieldMap
    ? (
      ({ data: Pick<F, MatchValues<Required<F>, M>> }) extends { data: infer D }
        ? (
          keyof D extends never
            ? {}
            : {
              [K in keyof D]: SchemaObjectTypeFieldFilterRecursion<STM, D[K], IsNever<MM> extends true ? M : MM, never>
            }
          )
        : never
      )
    : never;

export type SchemaObjectTypeFieldFilterRecursion<STM extends SchemaScalarTypeMap, S, M, MM> =
  S extends Thunk<SchemaScalarType>
    ? SchemaObjectTypeFieldFilterMatch<STM, S, M, MM>
    : (
      S extends SchemaFieldDefinition<infer T>
        ? Omit<S, 'type'> & { type: SchemaObjectTypeFieldFilterMatch<STM, T, M, MM> }
        : never
      );

export type SchemaObjectTypeFieldFilter<STM extends SchemaScalarTypeMap, S, M, MM> =
  S extends Thunk<SchemaScalarType>
    ? SchemaObjectTypeFieldFilterMatch<STM, S, M, MM>
    : (
      S extends SchemaFieldDefinition<infer T>
        ? Omit<S, 'type'> & { type: SchemaObjectTypeFieldFilterMatch<STM, T, M, MM> }
        : never
      );

/**
 * Filter schema type based on field definition
 */
export type SchemaObjectTypeFieldFilterMatch<STM extends SchemaScalarTypeMap, S, M, MM> =
  S extends Thunk<infer S>
    ? (
      S extends SchemaScalarType | SchemaEnumType
        ? S
        : (
          S extends SchemaObjectType<infer F>
            ? (
              S extends { connection: true, fields: { data: infer D } }
                // TODO: OmitData maintain only core and remove summary
                ? Omit<S, 'fields'> & { fields: { data: SchemaObjectTypeFieldFilter<STM, D, M, MM> } & Omit<F, 'data'> }
                : Omit<S, 'fields'> & {
                  fields: SchemaObjectTypeFieldMapFilterRecursion<STM, F, M, MM>
                }
            )
            : never
          )
    )
    : never;

/**
 * Format schema to default execution
 */
export type FormatSchemaDefaultOutput<STM extends SchemaScalarTypeMap, F> = SchemaObjectTypeFieldMapFilterRecursion<STM, F, { default: true } | { core: true }, never>;
export type FormatSchemaCoreOutput<STM extends SchemaScalarTypeMap, F> = SchemaObjectTypeFieldMapFilterRecursion<STM, F, { core: true }, { default: true } | { core: true }>;

export type FormatSchemaFieldOutput<STM extends SchemaScalarTypeMap, S, P> =
  S extends Thunk<SchemaScalarType>
    ? FormatSchemaOutput<STM, S, P>
    : (
      S extends SchemaFieldDefinition<infer T>
        ? Omit<S, 'type'> & { type: FormatSchemaOutput<STM, T, P> }
        : never
    );

export type FormatSchemaFieldMapOutput<STM extends SchemaScalarTypeMap, F, P> =
  F extends SchemaObjectTypeFieldMap
    ? (
      P extends { fields: {} }
        ? (
          Exclude<keyof P['fields'], Exclude<keyof P['fields'], keyof F>> extends never
            ? FormatSchemaDefaultOutput<STM, F>
            : (
              ({ data: Pick<F, Exclude<keyof F, Exclude<keyof F, keyof P['fields']>>> }) extends { data: infer D }
                ? (
                  keyof D extends string
                    ? (
                      ({ core: FormatSchemaCoreOutput<STM, F> }) extends { core: infer C }
                        ? ({
                            [K in keyof D]: FormatSchemaFieldOutput<STM, D[K], K extends keyof P['fields'] ? P['fields'][K] : never>
                          } & Omit<C, keyof D> & { [K in Exclude<keyof P['fields'], keyof F>]: { type: never } })
                        : never
                      )
                    : FormatSchemaDefaultOutput<STM, F>
                  )
                : never
              )
        )
        : FormatSchemaDefaultOutput<STM, F>
    )
    : never;

export type FormatSchemaOutput<STM extends SchemaScalarTypeMap, S, P> =
  S extends Thunk<infer S>
    ? (
      S extends SchemaScalarType | SchemaEnumType
        ? S
        : (
          S extends SchemaObjectType<infer F>
            ? (
              S extends { connection: true, fields: { data: infer D } }
                ? Omit<S, 'fields'> & {
                  fields: (
                    {
                      data: FormatSchemaFieldOutput<STM, D, P>
                    }
                    & (
                      P extends { modifiers: { summary: (infer SMF)[] } }
                        ? (
                          FormatSchemaCoreOutput<STM, Omit<F, 'data' | 'summary'>>
                          & (
                            F extends { summary: infer SM }
                              ? { summary: FormatSchemaFieldOutput<STM, SM, { fields: { [K in Extract<SMF, string>]: null } }> }
                              : {}
                            )
                          )
                        : FormatSchemaCoreOutput<STM, Omit<F, 'data'>>
                    )
                  );
                }
                : Omit<S, 'fields'> & { fields: FormatSchemaFieldMapOutput<STM, F, P> }
            )
            : never
        )
    )
    : never;


export type SchemaFields<S> =
  S extends Thunk<infer S>
    ? (
      S extends SchemaScalarType | SchemaEnumType
        ? never
        : (
          S extends SchemaObjectType<infer F>
            ? keyof F
            : never
        )
    )
    : never;

export type SchemaField<S, K extends string> =
  S extends Thunk<infer S>
    ? (
      S extends SchemaScalarType | SchemaEnumType
        ? never
        : (
          S extends SchemaObjectType<infer F>
            ? (
              F[K] extends SchemaFieldDefinition<infer T>
                ? T
                : F[K]
            )
            : never
        )
    )
    : never;

export type SchemaPathTuples =
  [string]
  | [string, string]
  | [string, string, string]
  | [string, string, string, string];

export type SchemaPath<S extends Thunk<SchemaObjectType<any>>, P extends SchemaPathTuples> =
  P extends [string]
    ? SchemaField<S, P[0]>
    : (P extends [string, string]
      ? SchemaField<SchemaField<S, P[0]>, P[1]>
      : (P extends [string, string, string]
        ? SchemaField<SchemaField<SchemaField<S, P[0]>, P[1]>, P[2]>
        : (P extends [string, string, string, string]
          ? SchemaField<SchemaField<SchemaField<SchemaField<S, P[0]>, P[1]>, P[2]>, P[3]>
          : never
        )
      )
    );

export type SchemaThunkObjectType<F> = Thunk<SchemaObjectType<F>>;

export function getSchemaPath<S extends Thunk<SchemaObjectType<any>>, P extends SchemaPathTuples>(type: S, ...path: P): SchemaPath<S, P> {
  let pathType = type;
  for (const field of path) {
    if (isObjectType(pathType)) {
      if (pathType.fields[field] != null) {
        pathType = getSchemaFieldType(pathType.fields[field]);
        continue;
      }
    }
    throw new Error(`Field ${field} does not exist on `);
  }
  return <any>pathType;
}

function scalarType<T = any, F = any>(decode: (value: F) => T, encode: (value: T) => F) {
  return { decode, encode };
}

export const schemaScalarTypes = {
  String: scalarType(
    value => `${value}`,
    value => value,
  ),
  Any: scalarType(
    value => value,
    value => value,
  ),
  Float: scalarType(
    value => parseFloat(value),
    value => value,
  ),
  Int: scalarType(
    value => parseInt(value, 10),
    value => value,
  ),
  ID: scalarType(
    value => `${value}`,
    value => value,
  ),
  DateTime: scalarType(
    value => new Date(Date.parse(value)),
    value => value.toISOString(),
  ),
  Boolean: scalarType(
    value => value === true,
    value => value,
  ),
};

export const Type: {
  [K in keyof typeof schemaScalarTypes]: K
} & {
  true: true,
  false: true,
  modifiers<T extends SchemaObjectInputTypeFieldMap>(modifiers: T): T;
  inputField<T extends SchemaInputTypeFieldDefinition>(field: T): T;
  outputField<T extends SchemaOutputTypeFieldDefinition>(field: T): T;
  ConnectionObjectType<F extends SchemaObjectOutputTypeFieldMap>(name: string, fields: F): SchemaObjectType<F> & { connection: true };
  ObjectType: {
    <T extends SchemaObjectType, F extends SchemaObjectOutputTypeFieldMap>(name: string, mixin: () => T[], fields: F): SchemaObjectType<T['fields'] & F>;
    <T extends SchemaObjectType>(name: string, mixin: () => T[]): SchemaObjectType<T['fields']>;
    <F extends SchemaObjectOutputTypeFieldMap>(name: string, fields: F): SchemaObjectType<F>;
  };
  InputType: {
    <T extends SchemaObjectType, F extends { [name: string]: SchemaInputTypeFieldDefinition } = {}>(name: string, mixin: () => T[], fields?: F): SchemaObjectType<T['fields'] & F>;
    <F extends { [name: string]: SchemaInputTypeFieldDefinition }>(name: string, fields: F): SchemaObjectType<F>;
  };
  EnumType<T extends EnumLike<number | string, StringKeyOf<T>>>(name: string, enumerator: T): SchemaEnumType<T>;
} = <any>(() => {
  const obj: any = {
    true: true,
    false: false,
    modifiers(modifiers: any) {
      return modifiers;
    },
    inputField(field: any) {
      return field;
    },
    outputField(field: any) {
      return field;
    },
    // tslint:disable-next-line
    ObjectType(name: string, ...args: any[]) {
      const getFields = () => {
        const fields: any = {};
        if (typeof args[0] === 'function') {
          const types = args[0]();
          for (const type of types) {
            for (const fieldName in type.fields) {
              if (type.fields.hasOwnProperty(fieldName)) {
                fields[fieldName] = type.fields[fieldName];
              }
            }
          }
          Object.assign(fields, args[1]);
        } else {
          Object.assign(fields, args[0]);
        }
        return fields;
      };
      const obj: any = { name };
      Object.defineProperty(obj, 'fields', {
        enumerable: true,
        configurable: true,
        get() {
          return getFields();
        },
      });
      return obj;
    },
    // tslint:disable-next-line
    ConnectionObjectType(name: string, ...args: any[]) {
      const obj = this.ObjectType(name, ...args);
      obj.connection = true;
      return obj;
    },
    // tslint:disable-next-line
    InputType(name: string, ...args: any[]) {
      return this.ObjectType(name, ...args);
    },
    // tslint:disable-next-line
    EnumType(name: string, enumerator: any) {
      return { name, enum: enumerator };
    },
  };
  for (const key in schemaScalarTypes) {
    if (schemaScalarTypes.hasOwnProperty(key)) {
      obj[key] = key;
    }
  }
  return obj;
})();


export function isThunkType<T = any>(type: any): type is () => T {
  return typeof type === 'function';
}

export function isScalarType(type: any): type is keyof SchemaScalarTypeMap {
  return Object.keys(schemaScalarTypes).includes(type);
}

export function isEnumType(type: any): type is SchemaEnumType {
  return typeof type === 'object' && type != null && 'enum' in type;
}

export function isObjectType<T = any>(type: any): type is SchemaObjectType<SchemaObjectTypeFieldMap<T>> {
  return typeof type === 'object' && type != null && 'fields' in type;
}

export function isObjectTypeFieldDefinition<T = any>(field: any): field is SchemaFieldDefinition<T> {
  return typeof field === 'object' && field != null && 'type' in field;
}

export function getSchemaType<T = any>(type: Thunk<T>): T {
  if (isThunkType<T>(type)) {
    return type();
  }
  return type;
}

export function getSchemaFieldType<T = any>(field: Thunk<Extract<T, string>> | SchemaFieldDefinition<T>): typeof field extends Thunk<Extract<infer R, string>> | SchemaFieldDefinition<infer R> ? R : never {
  if (isThunkType<T>(field)) {
    const innerType = getSchemaType<T>(field);
    if (isScalarType(innerType)) {
      return innerType;
    }
  } else if (isObjectTypeFieldDefinition<T>(field)) {
    return getSchemaType<T>(field.type);
  }
  throw new Error('Invalid schema field');
}

export function transformSchemaType<STM extends SchemaScalarTypeMap, T = any, DECODE extends true | false = true>(scalarTypes: STM, type: Thunk<T>, value: any, decode?: DECODE): TransformSchemaType<STM, Thunk<T>, DECODE> {
  if (isThunkType<T>(type)) {
    return transformSchemaType(scalarTypes, type(), value);
  }
  if (isScalarType(type)) {
    const scalarType = scalarTypes[type];
    if (value == null) {
      return <any>undefined;
    }
    const parse = (value: any) => {
      if (value == null) {
        return <any>undefined;
      }
      if (decode == null || decode === true) {
        return scalarType.decode(value);
      }
      return scalarType.encode(value)
    };
    if (Array.isArray(value)) {
      return <any>value.map(item => parse(item));
    }
    return parse(value);
  }
  if (isEnumType(type)) {
    if (value == null) {
      return <any>undefined;
    }
    if (Array.isArray(value)) {
      return <any>value.map((item: any) => type.enum[$enum(type.enum).getValueOrThrow(item)]);
    }
    return type.enum[$enum(type.enum).getValueOrThrow(value)];
  }
  if (isObjectType<T>(type)) {
    if (value == null) {
      return <any>undefined;
    }
    if (typeof value !== 'object' || value == null) {
      throw new Error(`Expected value object for object type`);
    }
    const parse = (value: any) => {
      const obj: any = {};
      for (const key in value) {
        if (value.hasOwnProperty(key)) {
          if (key === '__debug__') {
            obj[key] = value[key];
            continue;
          }
          if (!type.fields.hasOwnProperty(key)) {
            throw new Error(`key "${key}" was not defined on type "${type.name}"`);
          }
          const field = type.fields[key];
          let fieldType: TransformSchemaType<STM, Thunk<T>, DECODE>;
          if (isObjectTypeFieldDefinition<T>(field)) {
            fieldType = transformSchemaType(scalarTypes, field.type, value[key], decode);
          } else {
            fieldType = transformSchemaType(scalarTypes, field, value[key], decode);
          }
          obj[key] = fieldType;
        }
      }
      return obj;
    };
    if (Array.isArray(value)) {
      return <any>value.map(item => parse(item));
    }
    return parse(value);
  }
  throw new Error(`Unexpected schema type`);
}