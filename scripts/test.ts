import { exec } from '../internal/utils/exec';

const accessToken = (process.env.FB_ACCESS_TOKEN || '').trim();
let appSecret: string | undefined = (process.env.FB_APP_SECRET || '').trim();

if (appSecret === '') {
  appSecret = undefined;
}

if (accessToken.length === 0) {
  console.error(`You must specify "FB_ACCESS_TOKEN" env for testing client`);
  process.exit(2);
}

if (!module.parent) {
  exec('jest');
}


export {
  accessToken,
  appSecret,
};
