/**
 * This script builds to distribuition
 */
import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs-extra';
import { exec } from '../internal/utils/exec';

const distPath = pathResolve(appRootDir.get(), 'dist');
// First clear the build output dir.
fs.removeSync(distPath);
// Build typescript
exec('tsc');

// Copy documents
fs.copySync(
  pathResolve(appRootDir.get(), 'README.md'),
  pathResolve(distPath, 'README.md'),
);
// Clear our package json for release
const packageJson = require('../package.json');
packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;

fs.writeFileSync(
  pathResolve(distPath, './package.json'),
  JSON.stringify(packageJson, null, 2),
);

fs.removeSync(pathResolve(distPath, 'internal'));

fs.copySync(
  pathResolve(appRootDir.get(), 'LICENSE'),
  pathResolve(distPath, 'LICENSE'),
);
