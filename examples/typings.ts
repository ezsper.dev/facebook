import {
  FormatSchemaFieldOutput,
  SchemaFieldExecution,
  schemaScalarTypes,
  TransformSchemaFieldType
} from '../utils/schema';

import { UserType } from '../schema/UserType';

type DefaultExecution = SchemaFieldExecution<typeof schemaScalarTypes, { type: typeof UserType }>;

if (typeof global !== 'undefined' || typeof window !== 'undefined') {
  throw new Error('Not meant to be executed, only for typing illustration');
}

const defaultExecution = <DefaultExecution>{};

if (defaultExecution != null) {
  if (defaultExecution.fields != null) {
    if (defaultExecution.fields.accounts != null) {
      let accountModifiers = defaultExecution.fields.accounts.modifiers;
      accountModifiers = {
        limit: 1,
      };
    }
  }
}

type SchemaSelection = FormatSchemaFieldOutput<typeof schemaScalarTypes, { type: typeof UserType }, {
  fields: {
    accounts: {
      modifiers: {
        limit: 1,
        summary: ['totalCount'],
      },
      fields: {},
    },
  },
}>;

const schemaSelection = <SchemaSelection>{};

const result: TransformSchemaFieldType<typeof schemaScalarTypes, typeof schemaSelection> = <any>{};

const accounts = result.accounts.data[0];