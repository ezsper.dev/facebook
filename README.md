# Facebook Graph Client

This is a personal Facebook Graph Client with proper types for typescript.
<a href="https://stackblitz.com/edit/facebook-playground?file=request.ts" target="_blank">Try it out live!</a>

![Preview](https://gitlab.com/ezsper.com/facebook/raw/master/docs/assets/preview.gif)


> Still under development, not intended for production! 😁 

## Usage

```
import * as fb from '@ezsper/facebook';

const client = new fb.GraphClient({
  accessToken: '',
  appSecret: '',
});

const user = client.currentUser().get('/').execute({
  fields: {
    name: null,
    gender: null,
    accounts: {
      modifiers: {
        limit: 10,
      },
      fields: {
        category: null,
      },
    },
  },
});
```
