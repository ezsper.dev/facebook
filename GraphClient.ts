import * as crypto from 'crypto';
import * as qs from 'qs';
import * as superagent from 'superagent';
import {
  getSchemaFieldType,
  isObjectType,
  isObjectTypeFieldDefinition,
  transformSchemaType,
  SchemaFieldExecution,
  SchemaOutputTypeFieldDefinition,
  SchemaScalarTypeMap,
  TransformSchemaFieldType,
  FormatSchemaFieldOutput,
  schemaScalarTypes,
} from './utils/schema';
import { GraphError } from './GraphError';
import { UserPath } from './paths';
import * as stream from 'stream';
import * as fs from 'fs';

export interface GraphRequestOptions {
  accessToken?: string;
  appSecret?: string;
  debug?: string;
  suppressHttpCode?: boolean,
}

export type PathMap = {
  [path: string]: SchemaOutputTypeFieldDefinition;
};

export type NodeEndpoint = {
  [method: string]: PathMap;
};

export type ExecutionResult<STM extends SchemaScalarTypeMap, S, P> = TransformSchemaFieldType<STM, FormatSchemaFieldOutput<STM, S, P>>;

export interface GraphClientRequestCallback<T> {
  (err: any, res: GraphClientResponse<T>): void;
}

type Serializer = (obj: any) => string;

type BrowserParser = (str: string) => any;

type NodeParser = (res: superagent.Response, callback: (err: Error | null, body: any) => void) => void;

type Parser = BrowserParser | NodeParser;

type MultipartValueSingle = Blob | Buffer | fs.ReadStream | string | boolean | number;

type MultipartValue = MultipartValueSingle | MultipartValueSingle[];

export interface GraphClientRequest<T> extends Promise<GraphClientResponse<T>> {
  url: string;
  method: string;
  abort(): void;
  accept(type: string): this;
  attach(field: string, file: MultipartValueSingle, options?: string | { filename?: string; contentType?: string }): this;
  auth(user: string, pass: string, options?: { type: 'basic' | 'auto' }): this;
  auth(token: string, options: { type: 'bearer' }): this;
  buffer(val?: boolean): this;
  ca(cert: Buffer): this;
  cert(cert: Buffer | string): this;
  clearTimeout(): this;
  field(name: string, val: MultipartValue): this;
  field(fields: { [fieldName: string]: MultipartValue }): this;
  get(field: string): string;
  key(cert: Buffer | string): this;
  ok(callback: (res: Response) => boolean): this;
  on(name: 'error', handler: (err: any) => void): this;
  on(name: 'progress', handler: (event: ProgressEvent) => void): this;
  on(name: string, handler: (event: any) => void): this;
  parse(parser: Parser): this;
  part(): this;
  pfx(cert: Buffer | string | { pfx: Buffer, passphrase: string }): this;
  pipe(stream: NodeJS.WritableStream, options?: object): stream.Writable;
  query(val: object | string): this;
  redirects(n: number): this;
  responseType(type: string): this;
  retry(count?: number, callback?: GraphClientRequestCallback<T>): this;
  send(data?: string | object): this;
  serialize(serializer: Serializer): this;
  set(field: object): this;
  set(field: string, val: string): this;
  timeout(ms: number | { deadline?: number, response?: number }): this;
  type(val: string): this;
  unset(field: string): this;
  use(fn: Plugin): this;
  withCredentials(): this;
  write(data: string | Buffer, encoding?: string): this;
  end(callback?: GraphClientRequestCallback<T>): this;
}

export interface GraphClientResponse<T> extends NodeJS.ReadableStream {
  body: T;
  request: GraphClientRequest<T>;
  accepted: boolean;
  badRequest: boolean;
  charset: string;
  clientError: boolean;
  error: superagent.ResponseError;
  files: any;
  forbidden: boolean;
  get(header: string): string;
  header: any;
  info: boolean;
  links: object;
  noContent: boolean;
  notAcceptable: boolean;
  notFound: boolean;
  ok: boolean;
  redirect: boolean;
  serverError: boolean;
  status: number;
  statusType: number;
  text: string;
  type: string;
  unauthorized: boolean;
  xhr: XMLHttpRequest;
}

export const graphURL = 'https://graph.facebook.com';
export const graphVersion = '3.2';

const snakeCase: { (value: string): string } = require('lodash.snakecase');
const camelCase: { (value: string): string } = require('lodash.camelcase');
const isPlainObject: { (value: any): value is { [key: string]: any } } = require('lodash.isplainobject');

function camelCaseObject(value: any): any {
  if (Array.isArray(value)) {
    return value.map(item => camelCaseObject(item));
  }
  if (!isPlainObject(value)) {
    return value;
  }
  const camelObj: any = {};
  for (const key of Object.keys(value)) {
    if (value.hasOwnProperty(key)) {
      const camelKey = key.substr(0, 2) === '__' ? key : camelCase(key);
      camelObj[camelKey] = camelCaseObject(value[key]);
    }
  }
  return camelObj;
}

export function stringifySubFields<STM extends SchemaScalarTypeMap, F extends SchemaOutputTypeFieldDefinition, S extends SchemaFieldExecution<STM, F>>(scalarTypes: STM, field: F, execution: S): string {
  const modifiersQuery: string[] = [];
  const fields: string[] = [];
  if (execution == null) {
    throw new Error();
  }
  if (execution.modifiers != null) {
    for (const modifierName of Object.keys((<any>execution).modifiers)) {
      if ((<any>execution).modifiers.hasOwnProperty(modifierName)) {
        const modifier = (<any>execution).modifiers[modifierName];
        if (!isObjectTypeFieldDefinition(field) || (<any>field).modifiers == null || !(<any>field).modifiers.hasOwnProperty(modifierName)) {
          throw new Error(`Unknow modifier "${modifierName}"`);
        }
        const modifierField = (<any>field).modifiers[modifierName];
        let modifierType: any;
        if (isObjectTypeFieldDefinition(modifierField)) {
          modifierType = modifierField.type;
        } else {
          modifierType = modifierField;
        }
        const value = transformSchemaType(scalarTypes, modifierType, modifier, false);
        modifiersQuery.push(`${snakeCase(modifierName)}(${JSON.stringify(value)})`);
      }
    }
  }
  if (execution.fields != null) {
    for (const fieldName of Object.keys((<any>execution).fields)) {
      if ((<any>execution).fields.hasOwnProperty(fieldName)) {
        let type: any = getSchemaFieldType(field);
        if (!isObjectType(type)) {
          throw new Error(`Not an object type to select fields`);
        }
        if ((<any>type).connection === true) {
          type = getSchemaFieldType(type.fields.data);
        }
        if (!type.fields.hasOwnProperty(fieldName)) {
          throw new Error(`Object type "${type.name}" has no field "${fieldName}"`);
        }
        let fieldStr: string = snakeCase(fieldName);
        const def = (<any>execution).fields[fieldName];
        if (def != null) {
          fieldStr += stringifySubFields(scalarTypes, type.fields[fieldName], <any>def);
        }
        fields.push(fieldStr);
      }
    }
  }
  let stringified = '';
  if (modifiersQuery.length > 0) {
    stringified += `.${modifiersQuery.join('.')}`;
  }
  if (fields.length > 0) {
    stringified += `{${fields.join(',')}}`;
  }
  return stringified;
}


export function stringifyFields<STM extends SchemaScalarTypeMap, F extends SchemaOutputTypeFieldDefinition, S extends SchemaFieldExecution<STM, F>>(scalarTypes: STM, field: F, execution: S): { modifiers: any, fields: string | null } {
  if (execution == null) {
    return { modifiers: {}, fields: null };
  }

  const modifiersQuery: any = {};
  const fieldsQuery: string[] = [];

  if (execution.modifiers != null) {
    for (const modifierName of Object.keys((<any>execution).modifiers)) {
      if ((<any>execution).modifiers.hasOwnProperty(modifierName)) {
        const modifier = (<any>execution).modifiers[modifierName];
        if (field.modifiers == null || !field.modifiers.hasOwnProperty(modifierName)) {
          throw new Error(`Unknow modifier "${modifierName}"`);
        }
        const value: any = transformSchemaType(scalarTypes, field.modifiers[modifierName], modifier, false);
        const stringifyValue = (value: any) =>
          typeof value === 'object' && value != null ? JSON.stringify(value) : value;
        modifiersQuery[snakeCase(modifierName)] = Array.isArray(value)
          ? value.map(stringifyValue).join(',')
          : stringifyValue(value);
      }
    }
  }

  if (execution.fields != null) {
    for (const fieldName of Object.keys((<any>execution).fields)) {
      if ((<any>execution).fields.hasOwnProperty(fieldName)) {
        const type = getSchemaFieldType(field);
        if (!isObjectType(type)) {
          throw new Error(`Not an object type to select fields`);
        }
        if (!type.fields.hasOwnProperty(fieldName)) {
          console.warn(`Object type "${type.name}" has no field "${fieldName}"`);
        } else {
          const subfield = type.fields[fieldName];
          let fieldStr: string = snakeCase(fieldName);
          const def = (<any>execution).fields[fieldName];
          if (def != null) {
            fieldStr += stringifySubFields(scalarTypes, subfield, <any>def);
          }
          fieldsQuery.push(fieldStr);
        }
      }
    }
  }

  return { modifiers: modifiersQuery, fields: fieldsQuery.length > 0 ? fieldsQuery.join(',') : null };
}

function prepareUri<STM extends SchemaScalarTypeMap, F extends SchemaOutputTypeFieldDefinition, S extends SchemaFieldExecution<STM, F>>(scalarTypes: STM, field: F, path: string, options: S & GraphRequestOptions) {
  let uri = `${graphURL}/v${graphVersion}${path}`;

  let query: { [key: string]: any } = {};

  const {
    fields,
    modifiers: modifiersQuery,
  } = stringifyFields(scalarTypes, field, options);

  Object.assign(query, modifiersQuery);

  if (fields != null) {
    query.fields = fields;
  }

  if (options.accessToken != null) {
    if (query == null) {
      query = {};
    }
    query.access_token = options.accessToken;
    if (options.appSecret != null) {
      const hmac = crypto.createHmac('sha256', options.appSecret);
      hmac.update(options.accessToken);
      query.appsecret_proof = hmac.digest('hex');
    }
  }

  if (options.suppressHttpCode != null) {
    if (query == null) {
      query = {};
    }
    query.suppress_http_code = options.suppressHttpCode ? 1 : 0;
  }

  query.pretty = 0;

  query.format = 'json';

  if (options.debug != null) {
    if (query == null) {
      query = {};
    }
    query.debug = options.debug;
  }

  if (query != null) {
    uri += `?${qs.stringify(query)}`;
  }

  return uri;
}

export function makeRequest<STM extends SchemaScalarTypeMap, F extends SchemaOutputTypeFieldDefinition, S extends SchemaFieldExecution<STM, F>>(
  scalarTypes: STM,
  field: F,
  method: string,
  path: string,
  options: S & GraphRequestOptions,
): GraphClientRequest<ExecutionResult<STM, F, S>> {
  const uri = prepareUri(scalarTypes, field, path, options);
  const methodType: 'get' | 'post' | 'del' = <any>method.toLowerCase();
  const request = superagent[methodType](uri)
    .accept('json');

  const requestEnd = request.end;
  let response: any;
  // TODO retry
  const transformPromise = async () => {
    if (typeof response === 'undefined') {
      response = (
        new Promise<superagent.Response>((resolve, reject) => {
          requestEnd.call(request, (error: any, response: any) => {
            if (error != null) {
              reject(error);
            } else {
              resolve(response);
            }
          });
        })
      )
      .catch(error => {
        if ((<any>error).response != null && (<any>error).response.body != null) {
          const response = <superagent.Response>(<any>error).response;
          Object.defineProperty(response, 'originalError', {
            writable: true,
            configurable: true,
            value: error,
          });
          return response;
        }
        throw error;
      })
      .then((response) => {
        if (isPlainObject(response.body)) {
          response.body = camelCaseObject(response.body);
        }
        (<any>response).request = request;
        if (response.status !== 200) {
          if (isPlainObject(response.body) && 'error' in response.body) {
            const {
              message,
              type,
              code,
              fbtraceId,
            } = response.body.error;
            throw new GraphError(
              message,
              type,
              code,
              fbtraceId,
              response.status,
              response.body,
              response,
              (<any>response).originalError,
            );
          }
          if ((<any>response).originalError != null) {
            throw (<any>response).originalError;
          }
          throw new Error(JSON.stringify(response.body));
        }
        response.body = <any>transformSchemaType(scalarTypes, <any>getSchemaFieldType(field), response.body);
        return response;
      });
    }
    return response;
  };

  Object.defineProperties(request, {
    then: {
      writable: true,
      configurable: true,
      value: function _then(...args: any[]) {
        return transformPromise().then(...args);
      },
    },
    catch: {
      writable: true,
      configurable: true,
      value: function _catch(...args: any[]) {
        return transformPromise().catch(...args);
      },
    },
    finally: {
      writable: true,
      configurable: true,
      value: function _finally(...args: any[]) {
        return transformPromise().finally(...args);
      },
    },
    end: {
      writable: true,
      configurable: true,
      value: function end(callback: any) {
        transformPromise()
          .then(response => {
            callback(null, response);
          })
          .catch(error => {
            callback(error);
          });
      },
    },
  });

  return <any>request;
}

/*
 TODO Why is failing when check if its options
 type MethodExecution<STM extends SchemaScalarTypeMap, F> =
   {} extends SchemaFieldExecution<STM, F>
   ? DefaultMethodExecution<STM, F> & CustomMethodExecution<STM, F>
   : CustomMethodExecution<STM, F>;
 */

type RequestExecution<STM extends SchemaScalarTypeMap, F> =
  DefaultRequestExecution<STM, F> & CustomRequestExecution<STM, F>;

type MethodExecution<STM extends SchemaScalarTypeMap, F> =
  DefaultMethodExecution<STM, F> & CustomMethodExecution<STM, F>;

type DefaultMethodExecution<STM extends SchemaScalarTypeMap, F> =
  {
    (): ExecutionResult<STM, F, {}>;
  };

type CustomMethodExecution<STM extends SchemaScalarTypeMap, F> =
  {
    <P extends SchemaFieldExecution<STM, F>>(options: P & GraphRequestOptions): ExecutionResult<STM, F, P>;
  };

type DefaultRequestExecution<STM extends SchemaScalarTypeMap, F> =
  {
    (): GraphClientRequest<ExecutionResult<STM, F, {}>>;
  };

type CustomRequestExecution<STM extends SchemaScalarTypeMap, F> =
  {
    <P extends SchemaFieldExecution<STM, F>>(options: P & GraphRequestOptions): GraphClientRequest<ExecutionResult<STM, F, P>>;
  };

export class GraphClientExecution<STM extends SchemaScalarTypeMap, F extends SchemaOutputTypeFieldDefinition> {

  constructor(
    readonly scalarTypes: STM,
    readonly field: F,
    readonly method: string,
    readonly nodeId: string,
    readonly path: string,
    readonly options: GraphRequestOptions = {},
  ) {}

  request: RequestExecution<STM, F> = <any>((options?: any) => {
    return makeRequest(this.scalarTypes, this.field, this.method, `/${this.nodeId}${this.path}`, Object.assign(options, this.options, options));
  });

  execute: MethodExecution<STM, F> = <any>(async (options?: any) => {
    return (await this.request(options)).body;
  });

}


export class GraphNodeClient<STM extends SchemaScalarTypeMap, E extends NodeEndpoint> {

  constructor(readonly scalarTypes: STM, readonly nodeId: string, readonly endpoint: E, readonly options: GraphRequestOptions = {}) {}

  private getPath<M extends keyof E, P extends Extract<keyof E[M], string>>(method: M, path: P): E[M][P] {
    return this.endpoint[method][path];
  }

  get<P extends Extract<keyof E['GET'], string>>(path: P): GraphClientExecution<STM, E['GET'][P]> {
    return this.method('GET', path);
  }

  post<P extends Extract<keyof E['POST'], string>>(path: P): GraphClientExecution<STM, E['POST'][P]> {
    return this.method('POST', path);
  }

  del<P extends Extract<keyof E['DEL'], string>>(path: P): GraphClientExecution<STM, E['DEL'][P]> {
    return this.method('DEL', path);
  }

  method<M extends keyof E, P extends Extract<keyof E[M], string>>(method: M, path: P): GraphClientExecution<STM, E[M][P]> {
    return new GraphClientExecution(this.scalarTypes, this.getPath(method, path), <string>method, this.nodeId, path, this.options);
  }

}

type GetUser = (typeof UserPath)['GET']['/'];

export class GraphClient<STM extends SchemaScalarTypeMap = typeof schemaScalarTypes> {

  readonly scalarTypes: STM;

  constructor(options?: GraphRequestOptions)
  constructor(options: GraphRequestOptions, scalarTypes: STM)
  constructor(readonly options: GraphRequestOptions = {}, scalarTypes?: STM) {
    this.scalarTypes = scalarTypes == null ? (<any>schemaScalarTypes) : scalarTypes;
  }

  user(id: string) {
    return new GraphNodeClient(this.scalarTypes, id, UserPath, this.options);
  }

  currentUser() {
    return this.user('me');
  }

}



