import * as fb from '..';
import { accessToken } from '../scripts/test';

describe('User methods', () => {
  const client = new fb.GraphClient({
    accessToken,
  });

  it('get current user', async () => {
    const response = await client.currentUser().get('/').request({
      fields: {
        name: null,
        accounts: null,
      },
    });
    expect(typeof response.body.id).toBe('string');
    expect(typeof response.body.name).toBe('string');
    expect(Array.isArray(response.body.accounts.data)).toBe(true);
    const keys = Object.keys(response.body);
    expect(keys).toContain('accounts');
    expect(keys).toContain('name');
    expect(keys).toContain('id');
    const url = response.request.url.replace(accessToken, '*****');
    expect(url).toBe('https://graph.facebook.com/v3.2/me/?fields=name%2Caccounts&access_token=*****&pretty=0&format=json');
  });
});
